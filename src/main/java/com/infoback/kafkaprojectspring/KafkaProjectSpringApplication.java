package com.infoback.kafkaprojectspring;

import com.infoback.kafkaprojectspring.repositories.EventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaProjectSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaProjectSpringApplication.class, args);
	}

}
