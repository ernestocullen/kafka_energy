package com.infoback.kafkaprojectspring.beans;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class ProducerComponent {

    private KafkaProducer producer;

    public ProducerComponent() {
        this.producer = createProducer();
    }

    public KafkaProducer createProducer() {
        Properties props = new Properties();
        // reasonable defaults
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        // these should come from a properties file!
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:29092");
        props.put("schema.registry.url", "http://localhost:8090");
        return new KafkaProducer(props);
    }

    public KafkaProducer getProducer() {
        return producer;
    }
}
