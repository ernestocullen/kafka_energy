package com.infoback.kafkaprojectspring.clientdb;

import com.infoback.kafkaprojectspring.avro.EventBatchRecord;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class ClientDb {
    private static final String topic = "events_topic";

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:29092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        props.put("schema.registry.url", "http://localhost:8090");

        KafkaConsumer<String, EventBatchRecord> consumer = new KafkaConsumer<>(props);

        consumer.subscribe(Arrays.asList(topic));

        try {
            while (true) {
                ConsumerRecords<String, EventBatchRecord> records = consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, EventBatchRecord> record : records) {

                }
            }
        } finally {
            consumer.close();
        }
    }
}
