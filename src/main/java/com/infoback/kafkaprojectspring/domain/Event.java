package com.infoback.kafkaprojectspring.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name = "events")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @JsonProperty("device_id")
    private UUID deviceId;
    @JsonProperty("region")
    private Integer regionId;
    private int charging;

    public Event() { }

    public Event(UUID device_id, Integer region_id, int charging) {
        this.deviceId = device_id;
        this.regionId = region_id;
        this.charging = charging;
    }
}
