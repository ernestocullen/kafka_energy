package com.infoback.kafkaprojectspring.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.infoback.kafkaprojectspring.avro.EventBatchRecord;
import com.infoback.kafkaprojectspring.beans.ProducerComponent;
import com.infoback.kafkaprojectspring.domain.Event;
import com.infoback.kafkaprojectspring.repositories.EventsRepository;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Stream;


@RestController()
@RequestMapping("api")
public class EventController {

    final EventsRepository repo;
    private final Logger log = LoggerFactory.getLogger(EventController.class);
    private ObjectMapper mapper = new ObjectMapper();
    private final String topic = "events_topic";
    private final KafkaProducer producer;

    public EventController(EventsRepository repo, ProducerComponent producerComponent) {
        this.repo = repo;
        this.producer = producerComponent.getProducer();
    }

    @GetMapping("/events")
    public List<Event> getAllEvents() {
        return repo.findAll();
    }

    @PostMapping("/events")
    public ResponseEntity<Event> newEvent(@RequestBody Event event) {
        log.debug("Event received: {}", event);
        Event createdEvent = repo.save(event);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(createdEvent.getDeviceId())
            .toUri();

        return ResponseEntity.created(uri)
            .body(createdEvent);
    }

    @PostMapping("/eventbatches/{deviceId}")
    public ResponseEntity newEventBatch(HttpServletRequest request, @PathVariable("deviceId") String deviceId)
        throws IOException, ExecutionException, InterruptedException
    {
        // send the complete batch to kafka
        ByteBuffer body = ByteBuffer.wrap(IOUtils.toByteArray(request.getInputStream()));
        EventBatchRecord batch = new EventBatchRecord(deviceId, Instant.now().toEpochMilli(), body);
        ProducerRecord record = new ProducerRecord(topic, deviceId, batch);
        log.info("Sending record to kafka. DeviceId: {}", deviceId);
        Future<RecordMetadata> metadata = producer.send(record);
        return ResponseEntity.ok().body(serialize(metadata.get()));
    }

    protected Map<String, Object> serialize(RecordMetadata metadata) {
        return Map.of("offset", metadata.offset(),
            "partition", metadata.partition(),
            "topic", metadata.topic(),
            "timestamp", metadata.timestamp());
    }

    @PostMapping("/eventbatchesstring/{deviceId}")
    public ResponseEntity newEventBatchParse(@RequestBody String eventsBatch, @PathParam("deviceId") String deviceId) {
        // receive the batch as string, parse individual events and save to the database
        String[] events = eventsBatch.split("\\\\n");
        log.info("Received {} events", events.length);
        Stream.of(eventsBatch.split("\\\\n"))
            .forEach(s -> {
                try {
                    int lens = s.length();
                    s = s.substring(s.charAt(0) == '"' ? 1 : 0, s.charAt(lens - 1) == '"' ? lens - 1 : lens)
                        .replace("\\", "");
                    Event ev = mapper.readValue(s, Event.class);
                    ev = repo.save(ev);
                    log.info("Saving event {}", ev);
                } catch (JsonProcessingException e) {
                    log.error(e.toString());
                }
            });
        repo.flush();
        return ResponseEntity.ok().build();
    }

}
